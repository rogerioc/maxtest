package com.maxtest.rogerio.maxmilhastest.api.models;

/**
 * Created by rogerio on 20/05/2018.
 */
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fare implements Parcelable{

    @SerializedName("grossamount")
    @Expose
    private Integer grossamount;
    @SerializedName("totalbasefare")
    @Expose
    private Integer totalbasefare;
    @SerializedName("adultbasefare")
    @Expose
    private Integer adultbasefare;
    @SerializedName("totalfare")
    @Expose
    private Integer totalfare;
    @SerializedName("totalsurcharge")
    @Expose
    private Integer totalsurcharge;
    @SerializedName("adulttotalfare")
    @Expose
    private Integer adulttotalfare;
    @SerializedName("totalcommission")
    @Expose
    private String totalcommission;

    protected Fare(Parcel in) {
        if (in.readByte() == 0) {
            grossamount = null;
        } else {
            grossamount = in.readInt();
        }
        if (in.readByte() == 0) {
            totalbasefare = null;
        } else {
            totalbasefare = in.readInt();
        }
        if (in.readByte() == 0) {
            adultbasefare = null;
        } else {
            adultbasefare = in.readInt();
        }
        if (in.readByte() == 0) {
            totalfare = null;
        } else {
            totalfare = in.readInt();
        }
        if (in.readByte() == 0) {
            totalsurcharge = null;
        } else {
            totalsurcharge = in.readInt();
        }
        if (in.readByte() == 0) {
            adulttotalfare = null;
        } else {
            adulttotalfare = in.readInt();
        }
        totalcommission = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (grossamount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(grossamount);
        }
        if (totalbasefare == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalbasefare);
        }
        if (adultbasefare == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(adultbasefare);
        }
        if (totalfare == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalfare);
        }
        if (totalsurcharge == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalsurcharge);
        }
        if (adulttotalfare == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(adulttotalfare);
        }
        dest.writeString(totalcommission);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Fare> CREATOR = new Creator<Fare>() {
        @Override
        public Fare createFromParcel(Parcel in) {
            return new Fare(in);
        }

        @Override
        public Fare[] newArray(int size) {
            return new Fare[size];
        }
    };

    public Integer getGrossamount() {
        return grossamount;
    }

    public void setGrossamount(Integer grossamount) {
        this.grossamount = grossamount;
    }

    public Integer getTotalbasefare() {
        return totalbasefare;
    }

    public void setTotalbasefare(Integer totalbasefare) {
        this.totalbasefare = totalbasefare;
    }

    public Integer getAdultbasefare() {
        return adultbasefare;
    }

    public void setAdultbasefare(Integer adultbasefare) {
        this.adultbasefare = adultbasefare;
    }

    public Integer getTotalfare() {
        return totalfare;
    }

    public void setTotalfare(Integer totalfare) {
        this.totalfare = totalfare;
    }

    public Integer getTotalsurcharge() {
        return totalsurcharge;
    }

    public void setTotalsurcharge(Integer totalsurcharge) {
        this.totalsurcharge = totalsurcharge;
    }

    public Integer getAdulttotalfare() {
        return adulttotalfare;
    }

    public void setAdulttotalfare(Integer adulttotalfare) {
        this.adulttotalfare = adulttotalfare;
    }

    public String getTotalcommission() {
        return totalcommission;
    }

    public void setTotalcommission(String totalcommission) {
        this.totalcommission = totalcommission;
    }

}