package com.maxtest.rogerio.maxmilhastest.features.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.maxtest.rogerio.maxmilhastest.api.CreateServices;
import com.maxtest.rogerio.maxmilhastest.api.GoibiboApi;
import com.maxtest.rogerio.maxmilhastest.api.models.Flys;
import com.maxtest.rogerio.maxmilhastest.features.home.model.DataFly;


import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.maxtest.rogerio.maxmilhastest.helpers.HelperConvert.getMapByDataFly;

/**
 * Created by rogerio on 20/05/2018.
 */

public class HomeView extends ViewModel {
    private MutableLiveData<Flys> flys;
    private GoibiboApi api;
    public HomeView() {
        api = CreateServices.createService();
    }

    public LiveData<Flys> getFlys(DataFly dataFly) {
        Map<String,String> map = getMapByDataFly(dataFly);
        flys = new MutableLiveData<>();

        api.getFlys(map).enqueue(new Callback<Flys>() {
            @Override
            public void onResponse(Call<Flys> call, Response<Flys> response) {
                flys.postValue(response.body());
            }

            @Override
            public void onFailure(Call<Flys> call, Throwable t) {
                flys.setValue(null);
            }
        });

        return flys;
    }
}
