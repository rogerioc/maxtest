package com.maxtest.rogerio.maxmilhastest.helpers;

/**
 * Created by rogerio on 21/05/2018.
 */

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.maxtest.rogerio.maxmilhastest.api.models.Onwardflight;
import com.maxtest.rogerio.maxmilhastest.features.flys.ui.FlyDataBinding;
import com.maxtest.rogerio.maxmilhastest.features.home.model.DataFly;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
//&format=json&source=GRU&destination=PPB&dateofdeparture=20180521&seatingclass=E&adults=1&children=0&infants=0&counter=100
final public class HelperConvert {
    static public Map<String, String> getMapByDataFly(DataFly dataFly) {
        Map<String, String> map = new HashMap<>();
        map.put("format","json");
        map.put("source",dataFly.getSource());
        map.put("destination",dataFly.getDestination());
        map.put("dateofdeparture",dataFly.getDateGo());
        map.put("dateofarrival",dataFly.getDateArrive());
        map.put("seatingclass","E");
        map.put("adults", String.valueOf(dataFly.getPassengers()));
        map.put("children","0");
        map.put("infants","0");
        map.put("counter","100");
        return map;
    }

    @NonNull
    static public SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat("dd/MM/yyyy");
    }

    @NonNull
    static public SimpleDateFormat getCompressDateFormat() {
        return new SimpleDateFormat("yyyyMMdd");
    }

    public static Pair<FlyDataBinding,FlyDataBinding> flightToFliBinding(Onwardflight onwardflight) {
        FlyDataBinding flyDataBinding = getFlyDataBinding(onwardflight);
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        double price = onwardflight.getFare().getTotalbasefare()/100.0f;
        flyDataBinding.setPrice(numberFormat.format(price));
        FlyDataBinding flyBackDataBinding= getFlyDataBinding(onwardflight.getReturnfl().get(0));

        return new Pair<>(flyDataBinding,flyBackDataBinding);
    }

    @NonNull
    public static FlyDataBinding getFlyDataBinding(Onwardflight onwardflight) {
        FlyDataBinding flyDataBinding = new FlyDataBinding();
        flyDataBinding.setAirline(onwardflight.getAirline());
        flyDataBinding.setAirportArrive(onwardflight.getDestination());
        flyDataBinding.setAirportGo(onwardflight.getOrigin());
        flyDataBinding.setFlyNumber(onwardflight.getFlightcode());
        flyDataBinding.setTimeGo(onwardflight.getDeptime());
        flyDataBinding.setTimeArrive(onwardflight.getArrtime());
        flyDataBinding.setStops(new Integer(onwardflight.getStops()));
        flyDataBinding.setTimGoFly(onwardflight.getSplitduration());
        return flyDataBinding;
    }
}
