package com.maxtest.rogerio.maxmilhastest.features.flys.list;

import android.support.v7.widget.RecyclerView;

import com.maxtest.rogerio.maxmilhastest.databinding.LayoutFlyBinding;

/**
 * Created by rogerio on 26/05/2018.
 */

public class FlyHolder extends RecyclerView.ViewHolder  {
    LayoutFlyBinding layoutFlyBinding;

    public FlyHolder(LayoutFlyBinding itemView) {
        super(itemView.getRoot());
        layoutFlyBinding = itemView;
    }

    public LayoutFlyBinding getLayoutFlyBinding() {
        return layoutFlyBinding;
    }

    public FlyHolder setLayoutFlyBinding(LayoutFlyBinding layoutFlyBinding) {
        this.layoutFlyBinding = layoutFlyBinding;
        return this;
    }
}
