package com.maxtest.rogerio.maxmilhastest.api;

import android.support.annotation.NonNull;

import com.maxtest.rogerio.maxmilhastest.BuildConfig;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rogerio on 20/05/2018.
 */

public class CreateServices {
    static public GoibiboApi createService(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GoibiboApi.api)
                .addConverterFactory(GsonConverterFactory.create())
                .client(createIntercept().build())
                .build();
        return  retrofit.create(GoibiboApi.class);
    }

    @NonNull
    private static OkHttpClient.Builder createIntercept() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

//        if(BuildConfig.DEBUG) {
//            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//            httpClient.addInterceptor(interceptor);
//        }

        httpClient.addInterceptor(new Interceptor() {
                                      @Override
                                      public Response intercept(Chain chain) throws IOException {
                                          Request original = chain.request();
                                          HttpUrl originalHttpUrl = original.url();

                                          HttpUrl url = originalHttpUrl.newBuilder()
                                                  .addQueryParameter(GoibiboApi.QueryString.API_ID, BuildConfig.API_ID)
                                                  .addQueryParameter(GoibiboApi.QueryString.API_KEY,BuildConfig.API_KEY)
                                                  .build();

                                          Request request = original.newBuilder()
                                                  .method(original.method(), original.body())
                                                  .url(url)
                                                  .build();

                                          return chain.proceed(request);
                                      }
                                  });
        return httpClient;
    }
}
