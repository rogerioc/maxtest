package com.maxtest.rogerio.maxmilhastest.features.home.model;

/**
 * Created by rogerio on 21/05/2018.
 */
/*Map<String, String> map = new HashMap<>();
        //&format=json&source=GRU&destination=PPB&dateofdeparture=20180521&seatingclass=E&adults=1&children=0&infants=0&counter=100
        map.put("format","json");
        map.put("source","GRU");
        map.put("destination","PPB");
        map.put("dateofdeparture","20180521");
        map.put("seatingclass","E");
        map.put("adults","1");
        map.put("children","0");
        map.put("infants","0");
        map.put("counter","100");*/
public class DataFly {
    private String dateGo;
    private String dateArrive;
    private String source;
    private String destination;
    private int passengers;

    public String getDateGo() {
        return dateGo;
    }

    public DataFly setDateGo(String dateGo) {
        this.dateGo = dateGo;
        return this;
    }

    public String getDateArrive() {
        return dateArrive;
    }

    public DataFly setDateArrive(String dateArrive) {
        this.dateArrive = dateArrive;
        return this;
    }

    public String getSource() {
        return source;
    }

    public DataFly setSource(String source) {
        this.source = source;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public DataFly setDestination(String destination) {
        this.destination = destination;
        return this;
    }

    public int getPassengers() {
        return passengers;
    }

    public DataFly setPassengers(int passengers) {
        this.passengers = passengers;
        return this;
    }
}
