package com.maxtest.rogerio.maxmilhastest.api;

import com.maxtest.rogerio.maxmilhastest.api.models.Flys;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by rogerio on 20/05/2018.
 */

public interface GoibiboApi {
    static public final String api="http://developer.goibibo.com";
    @GET("/api/search/")
    Call<Flys> getFlys(@QueryMap Map<String, String> options);

    public class QueryString {
        public static final String API_ID = "app_id";
        public static final String API_KEY = "app_key";
    }
}
