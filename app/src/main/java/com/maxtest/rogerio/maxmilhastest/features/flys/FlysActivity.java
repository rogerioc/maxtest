package com.maxtest.rogerio.maxmilhastest.features.flys;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.maxtest.rogerio.maxmilhastest.R;
import com.maxtest.rogerio.maxmilhastest.api.models.Flys;
import com.maxtest.rogerio.maxmilhastest.databinding.ActivityFlysListBinding;
import com.maxtest.rogerio.maxmilhastest.features.flys.list.FlysAdapter;

import static com.maxtest.rogerio.maxmilhastest.common.ContextNames.FLYS;

/**
 * Created by rogerio on 22/05/2018.
 */

public class FlysActivity extends AppCompatActivity {

    public static final String tag=FlysActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flys_list);
        ActivityFlysListBinding activityFlysListBinding = DataBindingUtil.setContentView(this, R.layout.activity_flys_list);
        Intent intent = getIntent();
        if(intent!=null) {
            Flys flys = intent.getParcelableExtra(FLYS);
            activityFlysListBinding.recycleListFlight.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
            activityFlysListBinding.recycleListFlight.setHasFixedSize(true);
            activityFlysListBinding.recycleListFlight.setAdapter(new FlysAdapter(flys.getData().getOnwardflights()));
            Log.i(tag,flys.getData().toString());
        }


    }
}
