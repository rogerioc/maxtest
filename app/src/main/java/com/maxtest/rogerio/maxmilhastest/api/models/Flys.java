package com.maxtest.rogerio.maxmilhastest.api.models;

/**
 * Created by rogerio on 20/05/2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flys implements Parcelable{

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("data_length")
    @Expose
    private Integer dataLength;


    protected Flys(Parcel in) {
        data = in.readParcelable(Data.class.getClassLoader());
        if (in.readByte() == 0) {
            dataLength = null;
        } else {
            dataLength = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(data, flags);
        if (dataLength == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(dataLength);
        }
    }

    public static final Creator<Flys> CREATOR = new Creator<Flys>() {
        @Override
        public Flys createFromParcel(Parcel in) {
            return new Flys(in);
        }

        @Override
        public Flys[] newArray(int size) {
            return new Flys[size];
        }
    };

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getDataLength() {
        return dataLength;
    }

    public void setDataLength(Integer dataLength) {
        this.dataLength = dataLength;
    }

    @Override
    public int describeContents() {
        return 0;
    }


}