package com.maxtest.rogerio.maxmilhastest.features.home;

import android.app.DialogFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.maxtest.rogerio.maxmilhastest.R;
import com.maxtest.rogerio.maxmilhastest.api.models.Flys;
import com.maxtest.rogerio.maxmilhastest.common.DateType;
import com.maxtest.rogerio.maxmilhastest.databinding.ActivityMainBinding;
import com.maxtest.rogerio.maxmilhastest.features.flys.FlysActivity;
import com.maxtest.rogerio.maxmilhastest.features.home.model.DataFly;
import com.maxtest.rogerio.maxmilhastest.features.home.ui.HomeDataBinding;
import com.maxtest.rogerio.maxmilhastest.features.home.ui.SublimePickerFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.maxtest.rogerio.maxmilhastest.common.ContextNames.FLYS;
import static com.maxtest.rogerio.maxmilhastest.helpers.HelperConvert.getCompressDateFormat;
import static com.maxtest.rogerio.maxmilhastest.helpers.HelperConvert.getSimpleDateFormat;

public class MainActivity extends AppCompatActivity {
    private String tag=MainActivity.class.getSimpleName();
    SelectedDate mSelectedDate;
    ActivityMainBinding activityMainBinding;
    DateType dateType;

    HomeDataBinding homeDataBinding;
    private HomeView homeView;
    private DataFly dateFly;

    public HomeDataBinding getHomeDataBinding() {
        return homeDataBinding;
    }

    private Calendar dateGo,dateArrive;
    SublimePickerFragment.Callback mFragmentCallback = new SublimePickerFragment.Callback() {
        @Override
        public void onStart() {
            homeDataBinding.setLoading(false);
        }

        @Override
        public void onCancelled() {

        }


        @Override
        public void onDateTimeRecurrenceSet(SelectedDate selectedDate,
                                            int hourOfDay, int minute,
                                            SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                            String recurrenceRule) {

            mSelectedDate = selectedDate;
            SimpleDateFormat format = getSimpleDateFormat();

            String date=format.format(mSelectedDate.getStartDate().getTime()).toString();
            Calendar today = Calendar.getInstance();

            switch (dateType) {
                case GO: {
                    if(mSelectedDate.getStartDate().getTime().getTime() >= today.getTime().getTime()) {
                        homeDataBinding.setDateGo(date);
                        dateGo = mSelectedDate.getStartDate();
                    }else {
                        invalideDateGo();
                    }
                }break;
                case ARRIVE: {
                    if(mSelectedDate.getStartDate().getTime().getTime() >= dateGo.getTime().getTime()) {
                        homeDataBinding.setDateArrive(date);
                        dateArrive = mSelectedDate.getStartDate();
                    }else {
                        invalideDateArrive();
                    }

                }break;
            }

        }
    };

    public void invalideDateGo() {
        toast(R.string.invalidGoDate);
    }
    public void invalideDateArrive() {
        toast(R.string.invalidArriveDate);
    }

    public void toast(int stringInfo) {
        Toast.makeText(this,stringInfo,Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setHomeViewModel(this);
        homeDataBinding = new HomeDataBinding();
        homeView = ViewModelProviders.of(this).get(HomeView.class);

        setFlyDate();

    }

    private void setFlyDate() {
        SimpleDateFormat format = getSimpleDateFormat();
        Calendar calendar = Calendar.getInstance();
        dateGo = calendar;
        String date=format.format(calendar.getTime()).toString();
        homeDataBinding.setDateGo(date);

        Calendar calendarArrive = Calendar.getInstance();
        calendarArrive.add(Calendar.DATE, 1);
        dateArrive = calendarArrive;
        date=format.format(calendarArrive.getTime()).toString();
        homeDataBinding.setDateArrive(date);
        homeDataBinding.setSource("GRU");
        homeDataBinding.setDestination("PPB");
        homeDataBinding.setQuantPassangers("1");

    }

    public void dateClick(DateType dateType) {
        homeDataBinding.setLoading(true);
        this.dateType = dateType;
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(mFragmentCallback);

        // Options
        Pair<Boolean, SublimeOptions> optionsPair = getOptions();
        if(dateType==DateType.ARRIVE)
            optionsPair.second.setDateParams(dateArrive);

        if (!optionsPair.first) { // If options are not valid
            Toast.makeText(this, "No pickers activated",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        // Valid options
        Bundle bundle = new Bundle();
        pickerFrag.setArguments(bundle);
        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }


    // Validates & returns SublimePicker options
    Pair<Boolean, SublimeOptions> getOptions() {
        SublimeOptions options = new SublimeOptions();
        int displayOptions = 0;

        displayOptions = SublimeOptions.ACTIVATE_DATE_PICKER;

        options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);

        options.setDisplayOptions(displayOptions);

        // Enable/disable the date range selection feature
        //options.setCanPickDateRange(cbAllowDateRangeSelection.isChecked());

        // Example for setting date range:
        // Note that you can pass a date range as the initial date params
        // even if you have date-range selection disabled. In this case,
        // the user WILL be able to change date-range using the header
        // TextViews, but not using long-press.

        /*Calendar startCal = Calendar.getInstance();
        startCal.set(2016, 2, 4);
        Calendar endCal = Calendar.getInstance();
        endCal.set(2016, 2, 17);
        options.setDateParams(startCal, endCal);*/

        // If 'displayOptions' is zero, the chosen options are not valid
        return new Pair<>(displayOptions != 0 ? Boolean.TRUE : Boolean.FALSE, options);
    }

    public void search() {
        homeDataBinding.setLoading(true);
        SimpleDateFormat format = getCompressDateFormat();
        dateFly = new DataFly();
        dateFly.setDateArrive(format.format(dateArrive.getTime()));
        dateFly.setDateGo(format.format(dateGo.getTime()));
        dateFly.setSource(homeDataBinding.getSource());
        dateFly.setDestination(homeDataBinding.getDestination());
        dateFly.setPassengers(Integer.valueOf(homeDataBinding.getQuantPassangers()));
        homeView.getFlys(dateFly).observe(this, new Observer<Flys>() {
            @Override
            public void onChanged(@Nullable Flys flys) {
                homeDataBinding.setLoading(false);
                if(flys!=null || flys.getData().getOnwardflights()!=null) {
                    Log.d(tag, flys.getData().toString());
                    callNewFlysScreen(flys);
                }else {
                    Toast.makeText(MainActivity.this, R.string.not_found,Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void callNewFlysScreen(Flys flys) {
        Intent intent = new Intent(this,FlysActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(FLYS,flys);
        intent.putExtras(bundle);
        startActivity(intent);

    }
}
