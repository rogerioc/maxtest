package com.maxtest.rogerio.maxmilhastest.features.flys.ui;

import android.databinding.BaseObservable;

/**
 * Created by rogerio on 26/05/2018.
 */

public class FlyDataBinding extends BaseObservable {
    private String airline;
    private String timeGo;
    private String timGoFly;
    private String timeArrive;
    private String flyNumber;
    private String airportGo;
    private int stops;
    private String airportArrive;
    private String price;

    public String getAirline() {
        return airline;
    }

    public FlyDataBinding setAirline(String airline) {
        this.airline = airline;
        notifyChange();
        return this;
    }

    public String getTimeGo() {
        return timeGo;
    }

    public FlyDataBinding setTimeGo(String timeGo) {
        this.timeGo = timeGo;
        return this;
    }

    public String getTimGoFly() {
        return timGoFly;
    }

    public FlyDataBinding setTimGoFly(String timGoFly) {
        this.timGoFly = timGoFly;
        return this;
    }

    public String getTimeArrive() {
        return timeArrive;
    }

    public FlyDataBinding setTimeArrive(String timeArrive) {
        this.timeArrive = timeArrive;
        return this;
    }

    public String getFlyNumber() {
        return flyNumber;
    }

    public FlyDataBinding setFlyNumber(String flyNumber) {
        this.flyNumber = flyNumber;
        return this;
    }

    public String getAirportGo() {
        return airportGo;
    }

    public FlyDataBinding setAirportGo(String airportGo) {
        this.airportGo = airportGo;
        return this;
    }

    public int getStops() {
        return stops;
    }

    public FlyDataBinding setStops(int stops) {
        this.stops = stops;
        return this;
    }

    public String getAirportArrive() {
        return airportArrive;
    }

    public FlyDataBinding setAirportArrive(String airportArrive) {
        this.airportArrive = airportArrive;
        return this;
    }

    public String getPrice() {
        return price;
    }

    public FlyDataBinding setPrice(String price) {
        this.price = price;
        return this;
    }
}
