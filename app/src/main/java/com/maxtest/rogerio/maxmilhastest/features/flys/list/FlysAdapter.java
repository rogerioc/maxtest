package com.maxtest.rogerio.maxmilhastest.features.flys.list;

import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.maxtest.rogerio.maxmilhastest.api.models.Onwardflight;
import com.maxtest.rogerio.maxmilhastest.databinding.LayoutFlyBinding;
import com.maxtest.rogerio.maxmilhastest.features.flys.ui.FlyDataBinding;

import java.util.List;

import static com.maxtest.rogerio.maxmilhastest.helpers.HelperConvert.flightToFliBinding;


/**
 * Created by rogerio on 26/05/2018.
 */

public class FlysAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Onwardflight> flights;

    public FlysAdapter(List<Onwardflight> flights) {
        this.flights = flights;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutFlyBinding layoutFlyBinding = LayoutFlyBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FlyHolder(layoutFlyBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        android.support.v4.util.Pair<FlyDataBinding,FlyDataBinding> model = flightToFliBinding(flights.get(position));
        LayoutFlyBinding binding = ((FlyHolder) holder).getLayoutFlyBinding();

        //FlyDataBinding model =  binding.getFligth();
//        if(featuredView==null)
//            featuredView = new FeaturedView(model,handlerProgramm);
//        else
//            featuredView.setModel(model);
        binding.setFligth(model.first);
        binding.setFligthBack(model.second);
    }

    private void getFeaturedView(LayoutFlyBinding binding) {

    }

    @Override
    public int getItemCount() {
        return flights!=null?flights.size():0;
    }
}
