package com.maxtest.rogerio.maxmilhastest.api.models;

/**
 * Created by rogerio on 20/05/2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {

    @SerializedName("returnflights")
    @Expose
    private List<Object> returnflights = null;
    @SerializedName("onwardflights")
    @Expose
    private List<Onwardflight> onwardflights = null;


    protected Data(Parcel in) {
        onwardflights = in.createTypedArrayList(Onwardflight.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(onwardflights);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public List<Object> getReturnflights() {
        return returnflights;
    }

    public void setReturnflights(List<Object> returnflights) {
        this.returnflights = returnflights;
    }

    public List<Onwardflight> getOnwardflights() {
        return onwardflights;
    }

    public void setOnwardflights(List<Onwardflight> onwardflights) {
        this.onwardflights = onwardflights;
    }

}