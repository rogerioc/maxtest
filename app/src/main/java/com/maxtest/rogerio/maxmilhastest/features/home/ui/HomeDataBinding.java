package com.maxtest.rogerio.maxmilhastest.features.home.ui;

import android.databinding.BaseObservable;

/**
 * Created by rogerio on 21/05/2018.
 */
public class HomeDataBinding extends BaseObservable {
    private String dateGo;
    private String dateArrive;
    private Boolean loading;
    private String quantPassangers;
    private String source;
    private String destination;

    public Boolean getLoading() {
        return loading;
    }


    public String getSource() {
        return source;
    }

    public HomeDataBinding setSource(String source) {
        this.source = source;
        notifyChange();
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public HomeDataBinding setDestination(String destination) {
        this.destination = destination;
        notifyChange();
        return this;
    }

    public String getQuantPassangers() {
        return quantPassangers;
    }

    public HomeDataBinding setQuantPassangers(String quantPassangers) {
        this.quantPassangers = quantPassangers;
        notifyChange();
        return this;
    }

    public HomeDataBinding setLoading(Boolean loading) {
        this.loading= loading;
        notifyChange();
        return this;
    }

    public HomeDataBinding() {
        /*dateGo = new String();
        dateArrive = new String();
        loading = new ObservableField<>(false);
        quantPassangers = new ObservableField<>("1");
        source = new ObservableField<>("GRU");
        destination = new ObservableField<>("PPB");*/
    }

    public String getDateGo() {
        return dateGo;
    }

    public void setDateGo(String dateGo) {
        this.dateGo = dateGo;
        notifyChange();
    }

    public String getDateArrive() {
        return dateArrive;
    }

    public HomeDataBinding setDateArrive(String dateArrive) {
        this.dateArrive = dateArrive;
        notifyChange();
        return this;
    }
}
